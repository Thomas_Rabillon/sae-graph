#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code diffusé aux étudiants de BUT1 dans le cadre de la SAE 2.02: Exploration algorithmique d'un problème.

IUT d'Orleans BUT1 Informatique 2021-2022 
"""

import json
import networkx as nx
import matplotlib.pyplot as plt

import time

def collaborateurs_proches2(G, u, v):
    if u not in G.nodes or v not in G.nodes:
        print(u, "est un illustre inconnu")
        return None

    collaborateurs = set([u])
    collaborateursVisites = set()
    distance = 0

    while v not in collaborateurs:
        collaborateurs_directs = set()

        for c in collaborateurs:
            collaborateurs_directs.update(G.neighbors(c))

        collaborateursVisites.update(collaborateurs)
        collaborateurs = collaborateurs_directs - collaborateursVisites
        distance += 1

    return distance

def distance(G,u,v):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    
    Parametres:
        G: le graphe
        u: le sommet de départ
        k: la distance depuis u
    """
    if u not in G.nodes or v not in G.nodes:
        print(u,"est un illustre inconnu")
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    collaborateursVisites = set()
    distance = 0
    while v not in collaborateurs:
        collaborateurs_directs = set()
        for c in collaborateurs:
            for voisin in G.adj[c]:
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin)
        collaborateursVisites.add(c)
        collaborateurs = collaborateurs.union(collaborateurs_directs)
        collaborateurs -= collaborateursVisites
        distance += 1
    return distance

def centralite_sous_graph(G,Gc):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    
    Parametres:
        G: le graphe
        u: le sommet de départ
        k: la distance depuis u
    """
    centralite_max = -1
    acteur_central = None
    for acteur in G.nodes():
        if acteur not in Gc.nodes():
            distance_max = max(distance(G, acteur, autre_acteur) for autre_acteur in Gc.nodes())
            if distance_max > centralite_max:
                centralite_max = distance_max
                acteur_central = acteur
    
    return acteur_central



def collaborateurs_proches_origin(G,u,k):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    
    Parametres:
        G: le graphe
        u: le sommet de départ
        k: la distance depuis u
    """
    if u not in G.nodes:
        print(u,"est un illustre inconnu")
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    Gc = nx.Graph()
    for _ in range(k):
        collaborateurs_directs = set()
        for c in collaborateurs:
            for voisin in G.adj[c]:
                if voisin not in collaborateurs:
                    Gc.add_edge(c, voisin)
                    collaborateurs_directs.add(voisin)
        collaborateurs = collaborateurs.union(collaborateurs_directs)
    return Gc

G = nx.Graph()
with open("data/test.json", "r") as f:
    donnee = json.load(f)
    for dico in donnee:
        cast = dico['cast']
        for act1 in cast:
            for act2 in cast:
                G.add_edge(act1.replace('[', '').replace(']', ''), act2.replace('[', '').replace(']', ''))

def collaborateurs_communs(G, u, v):
    collab_u = distance(G, u, 1)
    collab_v = distance(G, v, 1)
    return {c for c in collab_u if c in collab_v}


def centralite_acteur(G, acteur):
    return max(nx.shortest_path_length(G, source=acteur).values())

def acteur_le_plus_central(G):
    centralites = {acteur: centralite_acteur(G, acteur) for acteur in G.nodes()}
    return min(centralites, key=centralites.get)

def distance_max_graph(G):
    return max(centralite_acteur(G,acteur) for acteur in G.nodes())


Gc = collaborateurs_proches_origin(G, 'Diana Koerner', 1)
print(centralite_sous_graph(G,Gc))
plt.figure(1,figsize=(20,10))
pos=nx.layout.kamada_kawai_layout(Gc)
nx.draw_networkx_labels(Gc, pos=pos)
nx.draw_networkx_nodes(Gc,pos=pos,node_size=900,edgecolors="black",linewidths=2)
nx.draw_networkx_edges(Gc,pos=pos,edgelist=Gc.edges)
plt.show()


# plt.figure(1,figsize=(20,10))
# pos=nx.layout.kamada_kawai_layout(G)
# nx.draw_networkx_labels(G, pos=pos)
# nx.draw_networkx_nodes(G,pos=pos,node_size=900,edgecolors="black",linewidths=2)
# nx.draw_networkx_edges(G,pos=pos,edgelist=G.edges)
# plt.show()
