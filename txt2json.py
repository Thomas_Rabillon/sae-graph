def txt2json(fic_txt, fic_json):
    with open(fic_txt, "r") as f:
        lignes = f.readlines()

    with open(fic_json, "w") as f:
        f.write("[\n")
        for i, l in enumerate(lignes):
            l = l.strip()
            if i != len(lignes)-1:
                f.write("   "+l+",\n")
            else:
                f.write("   "+l+"\n")
        f.write("]")

txt2json("data/data.txt", "data/data.json")
